﻿using UnityEngine;
using System.Collections;

public class TweetScheduler : MonoBehaviour {
	public string userName; 
	public Tweet[] tweets;
	private float tweetTime = 0f;
	private int nextTweet = 0;
	// Use this for initialization
	void Start () {
		ChangeAllUserName (userName);
	}
	
	// Update is called once per frame
	void Update () {
		tweetTime = TimelineManager.Instance.GetTime();
		if (nextTweet < tweets.Length) {
			if (tweetTime > tweets [nextTweet].scheduleTime) {
				TimelineManager.Instance.SendTweet (tweets [nextTweet]);
				nextTweet++;
			}
		}
	}

	void ChangeAllUserName(string name){
		foreach (Tweet tweet in tweets) {
			tweet.userName = name;
		}
	}
}
