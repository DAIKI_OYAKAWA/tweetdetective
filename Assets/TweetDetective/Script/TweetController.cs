﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TweetController : MonoBehaviour {
	public float newTweetTime = 1f;
	public Image userIcon;
	public Text userName;
	public Text tweetText;
	public Text tweetTime;
	public Image newTweetBack;
	public Image favoriteImage;
	public Sprite favoriteIcon;
	private float time;
	private Tweet myTweet;
	private bool isFavorite = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		float multiple = Mathf.Max (0f, 1f - time / newTweetTime);
		newTweetBack.color = new Color (
			newTweetBack.color.r,
			newTweetBack.color.g,
			newTweetBack.color.b,
			multiple);
	}

	public void SetTweetData(Tweet tweet){
		userIcon.sprite = tweet.userIcon;
		userName.text = tweet.userName;
		tweetText.text = tweet.mainText;
		float mainTime = TimelineManager.Instance.defaultTime + TimelineManager.Instance.GetTime ();
		int h = Mathf.FloorToInt (mainTime / 60f);
		string strH;
		if (h < 10) {
			strH = "0" + h;
		} else {
			strH = h.ToString ();
		}
		int m = Mathf.FloorToInt (mainTime % 60f);
		string strM;
		if (m < 10) {
			strM = "0" + m;
		} else {
			strM = m.ToString ();
		}
		tweetTime.text = strH + ":" + strM;
		myTweet = tweet;
	}

	public void OnPushedReply(){
		ReplyManager.Instance.BeginReply (myTweet);
	}

	public void OnPushedRetweet(){

	}

	public void OnPushedFavorite(){
		if (!isFavorite) {
			isFavorite = true;
			favoriteImage.sprite = favoriteIcon;
			FavoriteManager.Instance.AddFavoriteTweet (myTweet);
		}
	}

	public void OnSendReply(){
		ReplyManager.Instance.SendReply (myTweet);
	}
}
