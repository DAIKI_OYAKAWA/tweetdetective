﻿using UnityEngine;
using System.Collections;

public class LoadJsonFile : MonoBehaviour {
	public TextAsset jsonFile;
	// Use this for initialization
	void Start () {
		JSONObject jObj = new JSONObject (jsonFile.text);
		AccessData (jObj);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void AccessData(JSONObject obj){
		switch (obj.type) {
		case JSONObject.Type.OBJECT:
			for (int i = 0; i < obj.list.Count; i++) {
				string key = obj.keys [i];
				JSONObject j = obj.list [i];
				Debug.Log ("key:"+key+" "+(i+1)+"/"+obj.list.Count);
				AccessData (j);
			}
			break;
		case JSONObject.Type.ARRAY:
			for (int i = 0; i < obj.list.Count; i++) {
				JSONObject j = obj.list [i];
				Debug.Log ("ARRAY: "+(i+1)+"/"+obj.list.Count);
				AccessData (j);
			}
			break;
		case JSONObject.Type.STRING:
			Debug.Log ("str:"+obj.str);
			break;
		case JSONObject.Type.NUMBER:
			Debug.Log ("num:"+obj.n);
			break;
		case JSONObject.Type.BOOL:
			Debug.Log ("bool"+obj.b);
			break;
		case JSONObject.Type.NULL:
			Debug.Log ("NULL");
			break;
		default :
			Debug.Log ("NONE");
			break;
		}
	}
}
