﻿using UnityEngine;
using System.Collections;

public class PlayerAriaManager : SingletonMonoBehaviour<PlayerAriaManager> {
	public PlayerAriaController playerAriaController;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SendPlayer(string text){
		playerAriaController.OpenAria (text, 2f + text.Length * 0.2f);
	}
	public void ClosePlayer(){
		playerAriaController.CloseAria ();
	}
}
