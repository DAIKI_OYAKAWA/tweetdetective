﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerAriaController : MonoBehaviour {
	public float disableY;
	public Text playerText;
	private bool isVisible = false;
	private bool isCount = false;
	private float viewTime = 0f;
	private float viewTimeMax = 0f;
	private RectTransform myTransform;
	// Use this for initialization
	void Start () {
		myTransform = GetComponent<RectTransform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (isVisible) {
			myTransform.anchoredPosition = new Vector2 (
				myTransform.anchoredPosition.x,
				myTransform.anchoredPosition.y + (0 - myTransform.anchoredPosition.y) * 0.1f);
			if (viewTimeMax > 0) {
				viewTime += Time.deltaTime;
				if (viewTime > viewTimeMax) {
					CloseAria ();
				}
			}
		} else {
			myTransform.anchoredPosition = new Vector2 (
				myTransform.anchoredPosition.x,
				myTransform.anchoredPosition.y + (disableY - myTransform.anchoredPosition.y) * 0.1f);
		}
	}

	public void OpenAria(string text, float closeTime){
		playerText.text = text;
		isVisible = true;
		viewTimeMax = closeTime;
		viewTime = 0f;
	}

	public void CloseAria(){
		isVisible = false;
	}
}
