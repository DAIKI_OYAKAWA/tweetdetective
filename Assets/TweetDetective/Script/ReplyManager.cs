﻿using UnityEngine;
using System.Collections;

public class ReplyManager : SingletonMonoBehaviour<ReplyManager> {
	public MoveContents moveContents;
	public Sprite playerIcon;
	private bool isReply = false;
	private Tweet replyTarget;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void BeginReply(Tweet tweet){
		moveContents.SelectContents (1);
		moveContents.SelectHeader (1);
		isReply = true;
		replyTarget = tweet;
		PlayerAriaManager.Instance.SendPlayer ("このツイートはおかしい気がする・・・");
	}

	public void EndReply(){
		moveContents.SelectContents (0);
		moveContents.SelectHeader (0);
		isReply = false;
		PlayerAriaManager.Instance.ClosePlayer ();
	}

	public void SendReply(Tweet targetTweet){
		if (isReply) {
			Tweet tweet = new Tweet (playerIcon, "ひきこもり探偵", "@"+replyTarget.userName+" 矛盾している!! RT "+targetTweet.mainText);
			TimelineManager.Instance.SendTweet (tweet);
			EndReply ();
		}
	}

	public bool IsReply(){
		return isReply;
	}
}
