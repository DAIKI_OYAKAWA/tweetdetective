﻿using UnityEngine;
using System.Collections;

public class MoveContents : MonoBehaviour {
	public int selectContents = 0;
	public int selectHeader = 0;
	public RectTransform[] contents;
	public RectTransform[] headers;

	// Use this for initialization
	void Start () {
		SelectContents (selectContents);
		SelectHeader (selectHeader);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SelectContents(int type){
		selectContents = type;
		for (int i = 0; i < contents.Length; i++) {
			if (i == selectContents) {
				contents [i].gameObject.SetActive (true);
			} else {
				contents [i].gameObject.SetActive (false);
			}
		}
	}

	public void SelectHeader(int type){
		selectHeader = type;
		for (int i = 0; i < headers.Length; i++) {
			if (i == selectHeader) {
				headers [i].gameObject.SetActive (true);
			} else {
				headers [i].gameObject.SetActive (false);
			}
		}
	}
}
