﻿using UnityEngine;
using System.Collections;

public class TitleWallController : MonoBehaviour {
	public RectTransform moveObj;
	private bool isStart = false;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (isStart) {
			float y = moveObj.anchoredPosition.y;
			y += (moveObj.rect.height - y) * 0.1f;
			moveObj.anchoredPosition = new Vector2 (moveObj.anchoredPosition.x, y);
			if (Mathf.Abs (moveObj.rect.height - y) < 1f) {
				Destroy (gameObject);
			}
		}
	}

	public void OnPushedButton(){
		isStart = true;
		TimelineManager.Instance.StartTimeline ();
	}
}
