﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class User {
	public string name;
	public Sprite icon;
}
