﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimelineManager : SingletonMonoBehaviour<TimelineManager> {
	public Transform timelineObject;
	public GameObject tweetPrefab;
	public Scrollbar tweetScrollbar;
	public float defaultTime = 0f;
	public float autoTweetTime = 1f;
	public Sprite testIcon;
	private float mainTime = 0f;
	private float tweetTime = 0f;
	private bool isTweet = false;
	private bool isStartScroll = false;
	private bool isStartGame = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (isStartGame && !ReplyManager.Instance.IsReply()) {
			mainTime += Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.Space)) {
			mainTime += 10f;
		}
		if (!isStartScroll) {
			isStartScroll = true;
			tweetScrollbar.value = 1f;
		}
	}

	public void SendTweet(Tweet tweet){
		GameObject tweetObj = (GameObject)Instantiate (tweetPrefab);
		tweetObj.transform.SetParent (timelineObject, false);
		tweetObj.transform.SetAsFirstSibling ();
		tweetObj.GetComponent<TweetController> ().SetTweetData (tweet);
		isTweet = true;
	}

	void TestTweet(){
		tweetTime += Time.deltaTime;
		if (tweetTime > autoTweetTime) {
			if (tweetScrollbar.value == 1) {
				tweetTime = 0;
				SendTweet (new Tweet(testIcon,"俺","splatoon"));
				tweetScrollbar.value = 1f;
				isStartScroll = false;
			}
		}
	}

	public void OnScroll(){
		
	}

	public void StartTimeline(){
		isStartGame = true;
	}

	public float GetTime(){
		return mainTime;
	}
}
