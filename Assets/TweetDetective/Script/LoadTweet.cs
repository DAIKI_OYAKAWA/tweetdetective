﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class LoadTweet : MonoBehaviour {
	public TextAsset jsonFile;
	public string playerName = "ひとりごと";
	public User[] userList;
	private List<Tweet> tweetList = new List<Tweet>();
	private Tweet editTweet;
	private string editKey;
	private float tweetTime = 0f;
	private int nextTweet = 0;
	// Use this for initialization
	void Start () {
		JSONObject jObj = new JSONObject (jsonFile.text);
		AccessData (jObj);
	}

	// Update is called once per frame
	void Update () {
		tweetTime = TimelineManager.Instance.GetTime();
		if (nextTweet < tweetList.Count) {
			if (tweetTime > tweetList [nextTweet].scheduleTime) {
				if (tweetList [nextTweet].userName == playerName) {
					PlayerAriaManager.Instance.SendPlayer (tweetList [nextTweet].mainText);
				} else {
					TimelineManager.Instance.SendTweet (tweetList [nextTweet]);
				}
				nextTweet++;
			}
		}
	}

	void AccessData(JSONObject obj){
		switch (obj.type) {
		case JSONObject.Type.OBJECT:
			for (int i = 0; i < obj.list.Count; i++) {
				string key = obj.keys [i];
				editKey = key;
				JSONObject j = obj.list [i];
				AccessData (j);
			}
			break;
		case JSONObject.Type.ARRAY:
			Debug.Log ("Tweet: "+obj.list.Count);
			for (int i = 0; i < obj.list.Count; i++) {
				JSONObject j = obj.list [i];
				editTweet = new Tweet ();
				AccessData (j);
				tweetList.Add (editTweet);
			}
			break;
		case JSONObject.Type.STRING:
			if (editKey == "user_name") {
				editTweet.userName = obj.str;
				foreach (User user in userList) {
					if (obj.str == user.name) {
						editTweet.userIcon = user.icon;
						break;
					}
				}
			} else if (editKey == "description") {
				editTweet.mainText = obj.str;
			}
			break;
		case JSONObject.Type.NUMBER:
			if (editKey == "id") {
				editTweet.id = (int)obj.n;
			} else if (editKey == "time") {
				editTweet.scheduleTime = (int)obj.n;
			} else if (editKey == "reply_item") {
				editTweet.replyItem = (int)obj.n;
			} else if (editKey == "reply_id") {
				editTweet.replyId = (int)obj.n;
			} else if (editKey == "item_id") {
				editTweet.itemId = (int)obj.n;
			}
			break;
		case JSONObject.Type.BOOL:
			break;
		case JSONObject.Type.NULL:
			break;
		default :
			break;
		}
	}
}
