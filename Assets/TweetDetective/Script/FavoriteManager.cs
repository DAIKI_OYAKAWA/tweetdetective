﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FavoriteManager : SingletonMonoBehaviour<FavoriteManager> {
	public Transform favoriteLineObject;
	public GameObject tweetPrefab;
	public string[] itemList;
	public string[] tweetList;
	private List<Tweet> favoriteTweets = new List<Tweet>();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AddFavoriteTweet(Tweet tweet){
		favoriteTweets.Add (tweet);
		if (tweet.itemId == 0) {
			PlayerAriaManager.Instance.SendPlayer (tweetList [Random.Range (0, tweetList.Length)]);
		} else {
			AddFavoriteView (tweet);
			PlayerAriaManager.Instance.SendPlayer ("<color=yelow>"+itemList [tweet.itemId-1]+"</color>・・・これは！！");
		}
	}

	public void AddFavoriteView(Tweet tweet){
		GameObject tweetObj = (GameObject)Instantiate (tweetPrefab);
		tweetObj.transform.SetParent (favoriteLineObject, false);
		tweetObj.transform.SetAsFirstSibling ();
		tweetObj.GetComponent<TweetController> ().SetTweetData (tweet);
	}

}
