﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Tweet {
	public int id = 0;
	public Sprite userIcon;
	public string userName = "";
	public float scheduleTime = 0;
	public string mainText = "";
	public int replyItem = 0;
	public int replyId = 0;
	public int itemId = 0;
	public Tweet (){

	}
	public Tweet(Sprite icon, string name, string text){
		userIcon = icon;
		userName = name;
		mainText = text;
	}
}
