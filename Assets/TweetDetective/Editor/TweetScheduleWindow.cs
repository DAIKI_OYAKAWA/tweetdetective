﻿using UnityEngine;
using UnityEditor;
public class TweetScheduleWindow : EditorWindow {
	static TweetScheduleWindow myWindow;
	private Transform scheduleParent;
	private Transform oldScheduleParent;
	private bool[] userToggle;
	[MenuItem("Window/TweetScheduleWindow")]
	static void Open ()
	{
		if (myWindow == null) {
			myWindow = CreateInstance<TweetScheduleWindow> ();
		}
		myWindow.Show ();
	}

	void OnInspectorUpdate() {
		//Repaint ();
	}

	void OnGUI(){
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Schedule",GUILayout.Width(100));
		scheduleParent = (Transform)EditorGUILayout.ObjectField (scheduleParent, typeof(Transform));	
		EditorGUILayout.EndHorizontal ();
		if (scheduleParent) {
			TweetScheduler[] tweetCtrls = scheduleParent.GetComponentsInChildren<TweetScheduler> ();
			if (scheduleParent != oldScheduleParent || userToggle.Length != tweetCtrls.Length) {
				userToggle = new bool[tweetCtrls.Length];
				oldScheduleParent = scheduleParent;
			}
			for (int i = 0; i < tweetCtrls.Length; i++) {
				userToggle [i] = EditorGUILayout.ToggleLeft (tweetCtrls [i].userName, userToggle [i]);
			}
		}
	}
}
